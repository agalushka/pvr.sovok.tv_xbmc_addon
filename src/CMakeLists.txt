set(SOURCE_FILES
    addon.cpp
    helpers.h
    helpers.cpp
    sovok_pvr_client.h
    sovok_pvr_client.cpp
    sovok_tv.h
    sovok_tv.cpp
    input_buffer.h
    direct_buffer.h
    direct_buffer.cpp
    timeshift_buffer.h
    timeshift_buffer.cpp
)

if(WIN32)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
    set(WINDOWS_LIBRARIES ws2_32)
endif(WIN32)

if(UNIX)
    set(UNIX_LIBRARIES dl)
endif(UNIX)

include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_SOURCE_DIR}/include/xbmc)

add_library(pvr.sovok.tv SHARED ${SOURCE_FILES})

add_dependencies(pvr.sovok.tv Curl JsonCpp)

set_target_properties(pvr.sovok.tv PROPERTIES
    OUTPUT_NAME XBMC_Sovok.TV
)

target_link_libraries(pvr.sovok.tv
    platform
    json
    curl
    ${UNIX_LIBRARIES}
    ${WINDOWS_LIBRARIES}
)

install(TARGETS pvr.sovok.tv
    RUNTIME DESTINATION ${PROJECT_NAME}
    LIBRARY DESTINATION ${PROJECT_NAME}
)
