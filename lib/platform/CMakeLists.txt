if(WIN32)
    set(SOURCE_FILES
        windows/dlfcn-win32.h
        windows/dlfcn-win32.cpp
        windows/os-threads.h
        windows/os-threads.cpp
    )
endif(WIN32)

if(UNIX)
    set(SOURCE_FILES
        posix/serialport.cpp
    )
endif(UNIX)

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux" AND NOT ANDROID)
    find_library(RT_LIBRARY rt)
endif(${CMAKE_SYSTEM_NAME} MATCHES "Linux" AND NOT ANDROID)

if(APPLE)
    find_library(COREVIDEO_LIBRARY CoreVideo)
endif(APPLE)

find_package(Threads REQUIRED)

add_library(platform STATIC ${SOURCE_FILES})

target_link_libraries(platform
    ${RT_LIBRARY}
    ${COREVIDEO_LIBRARY}
    ${CMAKE_THREAD_LIBS_INIT}
)
