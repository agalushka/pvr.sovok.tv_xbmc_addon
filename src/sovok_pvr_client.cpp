/*
 *  Copyright (C) 2013 Alex Deryskyba (alex@codesnake.com)
 *  https://bitbucket.org/codesnake/pvr.sovok.tv_xbmc_addon
 *
 *  This file is part of pvr.sovok.tv XBMC Addon.
 *
 *  Foobar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with pvr.sovok.tv XBMC Addon. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <libXBMC_pvr.h>
#include "timeshift_buffer.h"
#include "direct_buffer.h"
#include "sovok_pvr_client.h"

using namespace std;
using namespace ADDON;

SovokPVRClient::SovokPVRClient(CHelper_libXBMC_addon *addonHelper, CHelper_libXBMC_pvr *pvrHelper,
                               const std::string &sovokLogin, const std::string &sovokPassword) :
    m_addonHelper(addonHelper),
    m_pvrHelper(pvrHelper),
    m_sovokTV(addonHelper, sovokLogin, sovokPassword),
    m_inputBuffer(NULL),
    m_isTimeshiftEnabled(false),
    m_shouldAddFavoritesGroup(true)
{

}

SovokPVRClient::~SovokPVRClient()
{
    CloseLiveStream();
}

PVR_ERROR SovokPVRClient::GetEPGForChannel(ADDON_HANDLE handle, const PVR_CHANNEL& channel, time_t iStart, time_t iEnd)
{
    EpgEntryList epgEntries = m_sovokTV.GetEpg(channel.iUniqueId, iStart, iEnd);
    EpgEntryList::const_iterator itEpgEntry = epgEntries.begin();
    for (int i = 0; itEpgEntry != epgEntries.end(); ++itEpgEntry, ++i)
    {
        EPG_TAG tag = { 0 };
        tag.iUniqueBroadcastId = channel.iUniqueId + i;
        tag.iChannelNumber = channel.iUniqueId;
        tag.strTitle = itEpgEntry->Title.c_str();
        tag.strPlot = itEpgEntry->Description.c_str();
        tag.startTime = itEpgEntry->StartTime;
        tag.endTime = itEpgEntry->EndTime;
        m_pvrHelper->TransferEpgEntry(handle, &tag);
    }

    return PVR_ERROR_NO_ERROR;
}

int SovokPVRClient::GetChannelGroupsAmount()
{
    size_t numberOfGroups = m_sovokTV.GetGroupList().size();
    if (m_shouldAddFavoritesGroup)
        numberOfGroups++;
    return numberOfGroups;
}

PVR_ERROR SovokPVRClient::GetChannelGroups(ADDON_HANDLE handle, bool bRadio)
{
    if (!bRadio)
    {
        PVR_CHANNEL_GROUP pvrGroup = { 0 };
        pvrGroup.bIsRadio = false;

        if (m_shouldAddFavoritesGroup)
        {
            strncpy(pvrGroup.strGroupName, "Избранное", sizeof(pvrGroup.strGroupName));
            m_pvrHelper->TransferChannelGroup(handle, &pvrGroup);
        }

        GroupList groups = m_sovokTV.GetGroupList();
        GroupList::const_iterator itGroup = groups.begin();
        for (; itGroup != groups.end(); ++itGroup)
        {
            strncpy(pvrGroup.strGroupName, itGroup->first.c_str(), sizeof(pvrGroup.strGroupName));
            m_pvrHelper->TransferChannelGroup(handle, &pvrGroup);
        }
    }

    return PVR_ERROR_NO_ERROR;
}

PVR_ERROR SovokPVRClient::GetChannelGroupMembers(ADDON_HANDLE handle, const PVR_CHANNEL_GROUP& group)
{
    if (m_shouldAddFavoritesGroup && group.strGroupName == std::string("Избранное"))
    {
        FavoriteList favorites = m_sovokTV.GetFavorites();
        FavoriteList::const_iterator itFavorite = favorites.begin();
        for (; itFavorite != favorites.end(); ++itFavorite)
        {
            PVR_CHANNEL_GROUP_MEMBER pvrGroupMember = { 0 };
            strncpy(pvrGroupMember.strGroupName, "Избранное", sizeof(pvrGroupMember.strGroupName));
            pvrGroupMember.iChannelUniqueId = *itFavorite;
            m_pvrHelper->TransferChannelGroupMember(handle, &pvrGroupMember);
        }
    }

    const GroupList &groups = m_sovokTV.GetGroupList();
    GroupList::const_iterator itGroup = groups.find(group.strGroupName);
    if (itGroup != groups.end())
    {
        std::set<int>::const_iterator itChannel = itGroup->second.Channels.begin();
        for (; itChannel != itGroup->second.Channels.end(); ++itChannel)
        {
            if (group.strGroupName == itGroup->first)
            {
                PVR_CHANNEL_GROUP_MEMBER pvrGroupMember = { 0 };
                strncpy(pvrGroupMember.strGroupName, itGroup->first.c_str(), sizeof(pvrGroupMember.strGroupName));
                pvrGroupMember.iChannelUniqueId = *itChannel;
                m_pvrHelper->TransferChannelGroupMember(handle, &pvrGroupMember);
            }
        }
    }

    return PVR_ERROR_NO_ERROR;
}

int SovokPVRClient::GetChannelsAmount()
{
    return m_sovokTV.GetChannelList().size();
}

PVR_ERROR SovokPVRClient::GetChannels(ADDON_HANDLE handle, bool bRadio)
{
    const ChannelList &channels = m_sovokTV.GetChannelList();
    ChannelList::const_iterator itChannel = channels.begin();
    for(; itChannel != channels.end(); ++itChannel)
    {
        const SovokChannel &sovokChannel = itChannel->second;
        if (bRadio == sovokChannel.IsRadio)
        {
            PVR_CHANNEL pvrChannel = { 0 };
            pvrChannel.iUniqueId = sovokChannel.Id;
            pvrChannel.iChannelNumber = sovokChannel.Id;
            pvrChannel.bIsRadio = sovokChannel.IsRadio;
            strncpy(pvrChannel.strChannelName, sovokChannel.Name.c_str(), sizeof(pvrChannel.strChannelName));

            string iconUrl = "http://sovok.tv" + sovokChannel.IconPath;
            strncpy(pvrChannel.strIconPath, iconUrl.c_str(), sizeof(pvrChannel.strIconPath));;

            m_pvrHelper->TransferChannelEntry(handle, &pvrChannel);
        }
    }

    return PVR_ERROR_NO_ERROR;
}

bool SovokPVRClient::OpenLiveStream(const PVR_CHANNEL& channel)
{
    string url = m_sovokTV.GetUrl(channel.iUniqueId, m_protectCode);
    if (url.empty())
        return false;

    try
    {
        if (m_isTimeshiftEnabled)
            m_inputBuffer = new TimeshiftBuffer(m_addonHelper, url, "special://userdata/addon_data/pvr.sovok.tv/timeshift.ts");
        else
            m_inputBuffer = new DirectBuffer(m_addonHelper, url);
    }
    catch (InputBufferException &)
    {
        return false;
    }

    return true;
}

void SovokPVRClient::CloseLiveStream()
{
    delete m_inputBuffer;
    m_inputBuffer = NULL;
}

int SovokPVRClient::ReadLiveStream(unsigned char* pBuffer, unsigned int iBufferSize)
{
    return m_inputBuffer->Read(pBuffer, iBufferSize);
}

long long SovokPVRClient::SeekLiveStream(long long iPosition, int iWhence)
{
    return m_inputBuffer->Seek(iPosition, iWhence);
}

long long SovokPVRClient::PositionLiveStream()
{
    return m_inputBuffer->GetPosition();
}

long long SovokPVRClient::LengthLiveStream()
{
    return m_inputBuffer->GetLength();
}

bool SovokPVRClient::SwitchChannel(const PVR_CHANNEL& channel)
{
    string url = m_sovokTV.GetUrl(channel.iUniqueId, m_protectCode);
    return m_inputBuffer->SwitchStream(url);
}

void SovokPVRClient::SetProtectCode(const std::string &protectCode)
{
    m_protectCode = protectCode;
}

void SovokPVRClient::SetTimeshiftEnabled(bool enable)
{
    m_isTimeshiftEnabled = enable;
}

void SovokPVRClient::SetAddFavoritesGroup(bool shouldAddFavoritesGroup)
{
    if (shouldAddFavoritesGroup != m_shouldAddFavoritesGroup)
    {
        m_shouldAddFavoritesGroup = shouldAddFavoritesGroup;
        m_pvrHelper->TriggerChannelGroupsUpdate();
    }
}
