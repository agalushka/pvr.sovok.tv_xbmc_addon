#pragma once
/*
 *  Copyright (C) 2013 Alex Deryskyba (alex@codesnake.com)
 *  https://bitbucket.org/codesnake/pvr.sovok.tv_xbmc_addon
 *
 *  This file is part of pvr.sovok.tv XBMC Addon.
 *
 *  Foobar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with pvr.sovok.tv XBMC Addon. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include "platform/threads/mutex.h"
#include "input_buffer.h"

namespace ADDON
{
    class CHelper_libXBMC_addon;
}

class DirectBuffer : public InputBuffer
{
public:
    DirectBuffer(ADDON::CHelper_libXBMC_addon *addonHelper, const std::string &streamUrl);
    ~DirectBuffer();

    long long GetLength() const;
    long long GetPosition() const;
    int Read(unsigned char *buffer, unsigned int bufferSize);
    long long Seek(long long iPosition, int iWhence) const;
    bool SwitchStream(const std::string &newUrl);

private:
    ADDON::CHelper_libXBMC_addon *m_addonHelper;
    void *m_streamHandle;
    PLATFORM::CMutex m_mutex;
};
